<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function index() {
    	return view('upload');
    }

    // public function store(Request $request) {
    // 	$request->file('image');

    // 	return $request->image->store('public');
    // }


    public function store(Request $request)
    {
    	if($request->hasFile('name')) {
    		// return $request->name->path();
    		// return $request->name->extension();
    		// return Storage::putFile('public',$request->file('name'));
    		// return $request->file('name')->store('public');

    		foreach ($request->name as $file) {
    			
    			$filename = $file->getClientOriginalName();
	    		$size	  = $file->getClientSize();

	    		$file->storeAs('public',$filename);

	    		$fileModal = new File;

	    		$fileModal->name = $filename;
	    		$fileModal->size = $size;
	    		$fileModal->save();
    		}

    		return "Image Save";

    	} else {
    		return "No file";
    	}

    }


    public function show() {
    	// return Storage::files('public');  //only public folder
    	// return Storage::allFiles('public'); //public folder and public folder sub folder

    	$url = Storage::url('flename.png');

    	return "<img src='".$url."'>";
    }


}
