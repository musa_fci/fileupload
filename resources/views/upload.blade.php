<!DOCTYPE html>
<html>
<head>
	<title>File Upload</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>


<body>

	<form action="/" method="post" enctype="multipart/form-data" class="form-inline">
		
		@csrf
  <div class="form-group">
    <label for="exampleFormControlFile1">Example file input</label>
    <!-- <input type="file" name="name" class="form-control-file" id="exampleFormControlFile1"> -->
    <input type="file" name="name[]" multiple="true" class="form-control-file" id="exampleFormControlFile1">
  </div>
		<button type="submit" class="btn btn-primary mb-2">Save</button>
	</form>

</body>
</html>